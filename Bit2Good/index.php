<?php include 'configuration.php' ?>
<?php get_header(); ?>

	<main role="main">
		<?php if($menu == 'vertical'){
			echo '<div class="b2g_left_main"> <nav class="nav nav-vertical" role="navigation">';
			html5blank_nav();
			echo '</nav></div><div class="b2g_right_main">';
		}
		?>		
		<!-- section -->
		<section>

			

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
		
		<?php if($menu == 'vertical'){echo'</div>';} ?>
	</main>

<?php get_footer(); ?>
