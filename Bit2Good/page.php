<?php include 'configuration.php' ?>
<?php get_header(); ?>

	<main role="main">
		<?php if($menu == 'vertical'){
			echo '<div class="b2g_left_main"> <nav class="nav nav-vertical" role="navigation">';
			html5blank_nav();
			echo '</nav></div><div class="b2g_right_main">';
		}
		?>	
		<!-- section -->
		<section>

			

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<?php if($menu == 'vertical'){echo'</div>';} ?>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
