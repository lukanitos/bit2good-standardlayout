(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		// SLIDER
		$('.bxslider').bxSlider();
		
		// DOM ready
		init();
		resizeMenu();
		
		// menu mobile toggle
		jQuery('.burger').click(function(){
			jQuery('.nav-mobile').slideToggle('slow');
			if(jQuery(this).hasClass('openMobileMenu')){
				jQuery(this).removeClass('openMobileMenu');
				jQuery('header').css('padding-bottom', '15px');
			} else{
				jQuery(this).addClass('openMobileMenu');
				jQuery('header').css('padding-bottom', '0px');
			}
		});
			
		// menu mobile submenu toggle
		jQuery('.nav-mobile .menu-item-has-children').click(function(){
			// open menu got clicked
			if(jQuery(this).hasClass('openSubmenu')){
				jQuery(this).children('.sub-menu').slideUp('slow');
				jQuery(this).removeClass('openSubmenu');			
			} else {
				jQuery('.nav-mobile .menu-item-has-children').each(function(){
					if(jQuery(this).hasClass('openSubmenu')){
						jQuery(this).children('.sub-menu').slideUp('slow');
						jQuery(this).removeClass('openSubmenu');
					}
				});
				jQuery(this).children('.sub-menu').slideDown('slow');
				jQuery(this).addClass('openSubmenu');				
			}
		});		
		
	});
	
})(jQuery, this);

jQuery( window ).resize(function() {
  resizeMenu();
});

function resizeMenu(){
	jQuery('.header_wrapper').css('width', jQuery('.wrapper').width());
	jQuery('.header').css('width', jQuery('.wrapper').width() -40);
	jQuery('.footer').css('width', jQuery('.wrapper').width());
	//jQuery('.wrapper').css('height', jQuery('main').height() + 331);
}

function init() {
    window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 50,
            header = document.querySelector("header");
			headerwrap = document.querySelector(".header_wrapper");
			main = document.querySelector("main");
        if (distanceY > shrinkOn && !classie.has(header,"smaller")) {
            classie.add(header,"smaller");
			classie.add(main,"smaller_main");
			classie.add(headerwrap,"header_wrapper_smaller");
        } else {
            if (distanceY < shrinkOn && classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
				classie.remove(main,"smaller_main");
				classie.remove(headerwrap,"header_wrapper_smaller");
            }
        }
    });
}