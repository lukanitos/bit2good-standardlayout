<?php include 'configuration.php' ?>
			<!-- footer -->
			<footer class="footer" role="contentinfo">

				<!-- copyright -->
				<p class="copyright">
					&copy; Copyright layout by <a href="http://www.bit2good.com" target="_blank"> Bit2Good.com </a> 
				</p>
				<!-- /copyright -->
				
				<!-- Socials -->
				<div class="b2g_social_container">
				<?php if(strlen($facebookUrl) > 10 ){
					echo '<a href="' . $facebookUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-facebook-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>
				<?php if(strlen($googlePlusUrl) > 10 ){
					echo '<a href="' . $googlePlusUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-google-plus-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>
				<?php if(strlen($twitterUrl) > 10 ){
					echo '<a href="' . $twitterUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-twitter-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>
				<?php if(strlen($xingUrl) > 10 ){
					echo '<a href="' . $xingUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-xing-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>
				<?php if(strlen($linkedInUrl) > 10 ){
					echo '<a href="' . $linkedInUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-linkedin-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>
				<?php if(strlen($pinterestUrl) > 10 ){
					echo '<a href="' . $pinterestUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-pinterest-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>	
				<?php if(strlen($youtubeUrl) > 10 ){
					echo '<a href="' . $youtubeUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-youtube-square" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>	
				<?php if(strlen($instagramUrl) > 10 ){
					echo '<a href="' . $instagramUrl . '#" class="b2g_social_link">';
					echo '<i class="fa fa-instagram" aria-hidden="true"></i>';
					echo '</a>';
				}	
				?>					
				</div>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
